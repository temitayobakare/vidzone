Hello {{ user.get_short_name }},

Please confirm your email by copying the URL below, and 
pasting it into your browser:

{{ protocol }}://{{ domain }}{{ url }}

Kind regards,
The WStreams Team
