from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import auth_login
from django.contrib.sites.shortcuts import get_current_site
from django.db import models, transaction
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import ugettext as _
from django.views.decorators.http import require_POST
from formtools.wizard.views import SessionWizardView
from .forms import PasswordChangeForm, ProfileForm, RegisterForm1, RegisterForm2


class RegisterView(SessionWizardView):
    template_name = 'registration/register.html'
    form_list = [
        ('name', RegisterForm1),
        ('password', RegisterForm2),
    ]

    def done(self, form_list, form_dict, **kwargs):
        name_form, pwd_form = form_dict['name'], form_dict['password']
        with transaction.atomic():
            # Save the new user:
            User = get_user_model()
            user = User(status=User.PHONE_VERIFIED, **name_form.cleaned_data)
            user.set_password(pwd_form.cleaned_data['password1'])
            user.save()
        # Log them in using the (default) model authentication backend:
        auth_login(self.request, user, settings.AUTHENTICATION_BACKENDS[0])
        return redirect('shows:index')

register_view = RegisterView.as_view()


def verify_email(request, fldb64=None, token=None):
    User = get_user_model()
    try:
        email = urlsafe_base64_decode(fldb64).decode()
        user = User.objects.get(email=email)
    except (TypeError, ValueError, User.DoesNotExist):
        user = None
    
    if user is not None and default_token_generator.check_token(user, token):
        # If token is valid, set email as verified:
        user.status |= User.EMAIL_VERIFIED
        user.save()
        return redirect('shows:index')
    else:
        return TemplateResponse(request, 'registration/verification_failed.html')
