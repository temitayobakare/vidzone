"use strict";

(function () {
    function VideoPlayer(container) {
        var title = container.dataset.title;
        var channel = container.dataset.channel;
        var src = container.dataset.src;
        var saveStats = container.hasAttribute('data-save-stats');
        var poster = container.dataset.poster;
        var trailer = container.dataset.trailer;
        var artwork128 = container.dataset.art128;
        var artwork512 = container.dataset.art512;
        var showId = parseInt(container.dataset.showId, 10);

        if (!src) {
            console.warn('Video source missing -- exiting.');
            return;
        }

        if (!poster) {
            console.warn('Video poster missing -- exiting.');
            return;
        }

        this._src = src;
        this._title = title;
        this._channel = channel;
        this._saveStats = saveStats;
        this._poster = poster;
        this._trailer = trailer;
        this._artwork128 = artwork128;
        this._artwork512 = artwork512;
        this._showId = showId;

        this._supportsMediaSession = ('mediaSession' in navigator);
        this._supportsOrientation = ('orientation' in navigator);
        this._supportsOrientationLock = (this._supportsOrientation && 
            ('lock' in window.screen.orientation));
        this._container = container;
        this._videoWidth = this._container.parentElement.clientWidth;
        this._videoHeight = Math.ceil((this._videoWidth * 9) / 16);
        this._player = null;
        this._ws = null;
        this._started = null;

        this.onError = this.onError.bind(this);
        this.onVideoStart = this.onVideoStart.bind(this);
        this.onVideoStop = this.onVideoStop.bind(this);
        this.onResize = this.onResize.bind(this);
        this.onSocketOpen = this.onSocketOpen.bind(this);
        this.onMessageReceived = this.onMessageReceived.bind(this);
        this.onSocketClose = this.onSocketClose.bind(this);
    }

    VideoPlayer.prototype.createPlayer = function () {
        if (this._player) {
            this._player.stop();
            this._player.destroy();
        }
        this._player = new Clappr.Player({
            parent: this._container,
            poster: this._poster,
            source: this._trailer || (location.protocol + this._src),
            height: this._videoHeight,
            width: this._videoWidth,
            disableVideoTagContextMenu: true,
            maxBufferLength: 10
        });
        if (this._showId) {
            this._player.on(Clappr.Events.PLAYER_ERROR, this.onError);
            this._player.on(Clappr.Events.PLAYER_PLAY, this.onVideoStart);
            // this._player.on(Clappr.Events.PLAYER_ENDED, this.onVideoStop);
            this._player.on(Clappr.Events.PLAYER_STOP, this.onVideoStop);
        }
    }

    VideoPlayer.prototype.createConnection = function () {
        var scheme = location.protocol === "https:" ? "wss" : "ws";

        if (this._showId) {
            this._ws = new WebSocket(scheme + '://' + location.host + '/live/' + this._showId);
            this._ws.addEventListener('open', this.onSocketOpen);
            this._ws.addEventListener('message', this.onMessageReceived);
            this._ws.addEventListener('close', this.onSocketClose);
    
            if (this._ws.readyState === WebSocket.OPEN) {
                this._ws.onopen();
            }
        }
    }

    VideoPlayer.prototype.load = function (source, mimeType, autoPlay) {
        if (!source) {
            source = location.protocol + this._src;
            mimeType = 'application/vnd.apple.mpegurl';
        }
        if (this._player) {
            console.debug('Loading media from', source);
            this._player.load(source, mimeType, !!autoPlay);
        } else {
            console.warn('Load called on null Player instance.');
        }
    }

    VideoPlayer.prototype.setMediaSessionData = function () {
        if (this._supportsMediaSession && this._artwork128 && this._artwork512) {
            navigator.mediaSession.metadata = new MediaMetadata({
                title: this._title,
                album: this._channel,
                artwork: [
                    {src: this._artwork128, sizes: '128x128 256x256', type: 'image/png'},
                    {src: this._artwork512, sizes: '512x512', type: 'image/png'},
                ]
            });
        }
    }

    VideoPlayer.prototype.getCurrentLocation = function () {
        if (typeof this._location === 'undefined') {
            var self = this;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (pos) {
                    self._location = {
                        lat: pos.coords.latitude,
                        lng: pos.coords.longitude
                    };
                    console.debug('Location:', self._location);
                }, function (err) {
                    self._location = null;
                    console.warn('Location unavailable for this user.');
                }, {
                    enableHighAccuracy: false,
                    maximumAge: 30000,
                    timeout: 27000
                });
            } else {
                this._location = null;
            }
        }
    }

    VideoPlayer.prototype.play = function () { this._player.play(); }
    VideoPlayer.prototype.pause = function () { this._player.pause(); }
    VideoPlayer.prototype.stop = function () { this._player.stop(); }

    VideoPlayer.prototype.onError = function (evt) {
        var error = evt.error;
        console.error('Error:', evt.name, 'Detail:', error);
    }

    VideoPlayer.prototype.onVideoStart = function (evt) {
        this.getCurrentLocation();
        this.setMediaSessionData();

        if (this._saveStats && !this._started) {
            // Record start time, so we know how long the user watched this show:
            this._started = new Date();
            console.debug('Started video at:', this._started);
        }
    }

    VideoPlayer.prototype.onVideoStop = function (evt) {
        if (this._saveStats) {
            var duration = (new Date()).getTime() - this._started.getTime();
            var data = {show_id: this._showId, duration: duration};
            if (this._location) {
                data.lat = this._location.lat;
                data.lng = this._location.lng;
            }

            console.debug('Sending:', data);
            this._ws.send(JSON.stringify({
                stream: 'show',
                payload: data
            }));
            this._started = null;
        }
    }
    
    VideoPlayer.prototype.onResize = function (evt) {
        if (this._player) {
            this._videoWidth = this._container.parentElement.clientWidth;
            this._videoHeight = Math.ceil((this._videoWidth * 9) / 16);
            this._player.resize({
                width: this._videoWidth,
                height: this._videoHeight
            });
        }
    }

    VideoPlayer.prototype.onSocketOpen = function () { console.debug('WebSocket connected.'); }
    VideoPlayer.prototype.onSocketClose = function () { console.debug('WebSocket disconnected.'); }

    VideoPlayer.prototype.onMessageReceived = function (evt) {
        var data = JSON.parse(evt.data);
        var msg = data.payload;
        console.debug('Received:', data);
        switch (data.stream) {
            case 'show':
                if (msg.event === 'publish') {
                    this.load();
                } else if (msg.event === 'publish_done') {
                    this.stop();
                }
                break;
            case 'chat':
                videoComments.receiveComment(msg);
                break;
        }
    }

    window.addEventListener('DOMContentLoaded', function () {
        var container = document.getElementById('video-container');
        var videoPlayer = new VideoPlayer(container);
        videoPlayer.createPlayer();
        videoPlayer.createConnection();
        if (!videoPlayer._trailer) {
            // Stream is already in progress -- start playing:
            videoPlayer.play();
        }
    
        window.addEventListener('orientationchange', videoPlayer.onResize);
        window.addEventListener('resize', videoPlayer.onResize);
    
        window.VideoPlayer = VideoPlayer;
        window.videoSocket = videoPlayer._ws;
        window.videoPlayer = videoPlayer;
    });
}());
