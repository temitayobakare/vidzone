from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import REDIRECT_FIELD_NAME, redirect_to_login as redirect_to_url
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.utils.timezone import now
from django.utils.translation import ugettext as _
from .forms import ChannelForm, ShowForm
from .models import Channel, Show
import logging


log = logging.getLogger(__name__)


def index(request):
    all_shows = Show.objects.select_related('channel')
    selected = all_shows.order_by('-created')[:9]
    featured = all_shows.featured()
    featured_channel = Channel.objects.featured()
    return TemplateResponse(request, 'shows/index.html', 
        {'selected': selected, 'featured': featured, 'featured_channel': featured_channel})


@login_required
def broadcast(request):
    try:
        shows = request.user.channel.shows.all()
        return (redirect('shows:new_show') if not shows.exists() else 
            TemplateResponse(request, 'shows/shows.html', {'shows': shows}))
    except Channel.DoesNotExist:
        return redirect_to_url(reverse('shows:new_show'), reverse('shows:new_channel'))


@login_required
def channel(request):
    try:
        channel, new = Channel.objects.get(owner=request.user), False
    except Channel.DoesNotExist:
        channel, new = None, True
    if request.method == 'POST':
        form = ChannelForm(request.POST, request.FILES, instance=channel, request=request)
        if form.is_valid():
            channel = form.save()
            url = request.GET.get(REDIRECT_FIELD_NAME, reverse('shows:manage_channel'))
            messages.success(request, _('Your Channel has been %(action)s.' % {
                'action': _('created') if new else _('updated')
            }))
            return redirect(url)
    else:
        form = ChannelForm(instance=channel, request=request)
    return TemplateResponse(request, 'shows/channel.html', {'form': form, 'channel': channel})


@login_required
def show(request, show_name=None):
    show = get_object_or_404(Show, stream_name=show_name, channel__owner=request.user) if show_name else None
    if request.method == 'POST':
        form = ShowForm(request.POST, request.FILES, instance=show, request=request)
        if form.is_valid():
            show = form.save()
            messages.success(request, _('"%(show)s" saved. You\'re ready to start watching.') % {'show': show})
            return redirect('shows:manage_show', show_name=show.stream_name)
    else:
        form = ShowForm(instance=show, request=request)
    return TemplateResponse(request, 'shows/show.html', {'form': form, 'show': show})


def watch(request, channel, show_name):
    show = get_object_or_404(Show.objects.select_related('channel'), stream_name=show_name)
    return TemplateResponse(request, 'shows/watch.html', {'show': show,})
