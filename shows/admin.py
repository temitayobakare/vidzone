from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from .models import (Channel, License, Show)

User = get_user_model()


class ChannelAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'created', 'num_shows',)
    list_filter = ('created',)

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(
            num_shows=models.Count('shows'))

    def num_shows(self, obj):
        return obj.num_shows
    num_shows.short_description = _('Shows')
    num_shows.admin_order_field = 'num_shows'


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('name', 'url',)


admin.site.register(Channel, ChannelAdmin)
admin.site.register(License, LicenseAdmin)
